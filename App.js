import React, { Component } from "react";
import { StyleSheet, Text, View } from "react-native";
import { Provider } from "react-redux";
import store from "./src/store";
import Navigation from "./src/Navigation";

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <View style={{ flex: 1, paddingTop: 30 }}>
          <Navigation />
        </View>
      </Provider>
    );
  }
}

export default App;
