export const saveExchange = (state, payload) => {
  const index = state.findIndex(item => item.id === payload.id);
  if (index > -1) {
    return state.map(item => (item.id === payload.id ? payload : item));
  }
  return [...state, payload];
};
