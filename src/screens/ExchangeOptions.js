import React, { Component } from "react";
import { connect } from "react-redux";
import { View, Text, Picker } from "react-native";
import { Header, Button, FormLabel, FormInput, FormValidationMessage } from "react-native-elements";
import { saveExchange } from "../actions/exchange";

class ExchangeOptions extends Component {
  state = {
    id: null,
    title: "",
    key: "",
    secret: "",
    exchange: ""
  };
  componentWillMount() {
    const { id, action } = this.props.navigation.state.params;
    let exchange = {};
    if (action === "EDIT") {
      exchange = this.props.data.find(item => {
        return item.id === id ? item : null;
      });
      this.setState(exchange);
    } else if (action === "ADD") {
      this.setState({ id });
    }
  }
  render() {
    return (
      <View>
        <FormLabel>Name</FormLabel>
        <FormInput
          value={this.state.title}
          autoCorrect={false}
          onChangeText={text => this.setState({ title: text })}
        />
        <FormLabel>Key</FormLabel>
        <FormInput
          value={this.state.key}
          autoCorrect={false}
          onChangeText={text => this.setState({ key: text })}
        />
        <FormLabel>Secret</FormLabel>
        <FormInput
          value={this.state.secret}
          autoCorrect={false}
          onChangeText={text => this.setState({ secret: text })}
        />
        <FormLabel>Exchange</FormLabel>
        <Picker
          selectedValue={this.state.exchange}
          onValueChange={(itemValue, itemIndex) => this.setState({ exchange: itemValue })}
        >
          <Picker.Item label="gdax" value="gdax" />
          <Picker.Item label="bittrex" value="bittrex" />
          <Picker.Item label="kraken" value="kraken" />
          <Picker.Item label="bitfinex" value="bitfinex" />
        </Picker>
        <Button
          icon={{ name: "done", size: 32 }}
          buttonStyle={{ backgroundColor: "red" }}
          textStyle={{ textAlign: "center" }}
          title={`Save`}
          onPress={() => {
            this.props.saveExchange(this.state);
            this.props.navigation.goBack();
          }}
        />
      </View>
    );
  }
}

const mapStateToProps = state => ({
  data: state.exchange
});

export default connect(mapStateToProps, { saveExchange })(ExchangeOptions);
