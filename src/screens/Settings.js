import React, { Component } from "react";
import { connect } from "react-redux";
import { StyleSheet, View, ScrollView } from "react-native";
import { Header, Icon, Text, Button, List, ListItem } from "react-native-elements";
import { removeExchange } from "../actions/exchange";
import { ID } from "../utils";

class Settings extends Component {
  static navigationOptions = {
    title: "Settings",
    tabBarLabel: "Settings",
    tabBarIcon: ({ tintColor }) => <Icon name="settings" />
  };
  render() {
    return (
      <View>
        <ScrollView>
          <Text h2 style={{ fontSize: 20 }}>
            Exchanges
          </Text>
          <List containerStyle={{ marginBottom: 20 }}>
            {this.props.data.map(item => (
              <ListItem
                key={item.id}
                leftIcon={{ name: "mode-edit" }}
                leftIconOnPress={() => {
                  this.props.navigation.navigate("ExchangeOptions", {
                    id: item.id,
                    action: "EDIT"
                  });
                }}
                rightIcon={{ name: "delete" }}
                onPressRightIcon={() => {
                  this.props.removeExchange(item.id);
                }}
                title={item.title}
              />
            ))}
          </List>
          <Button
            title="Add"
            onPress={() => {
              this.props.navigation.navigate("ExchangeOptions", {
                id: ID(),
                action: "ADD"
              });
            }}
          />
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  data: state.exchange
});

export default connect(mapStateToProps, { removeExchange })(Settings);
