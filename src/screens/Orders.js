import React, { Component } from "react";
import { connect } from "react-redux";
import { StyleSheet, View, ScrollView } from "react-native";
import { Header, Icon, Text, Button } from "react-native-elements";
import List from "../components/List";

class Orders extends Component {
  static defaultProps = {
    data: []
  };
  static navigationOptions = {
    title: "Orders",
    tabBarLabel: "Orders",
    tabBarIcon: ({ tintColor }) => <Icon name="settings" />
  };
  config = {
    rightIcon: { name: "delete" },
    rightIconOnPress: () => {}
  };
  render() {
    return (
      <View>
        <ScrollView>
          <Text h2 style={{ fontSize: 20 }}>
            Orders
          </Text>
          <List data={this.props.data} config={this.config} />
          <Button
            title="Add"
            onPress={() => {
              this.props.navigation.navigate("ExchangeOptions");
            }}
          />
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  data: state.orders
});

export default connect(mapStateToProps)(Orders);
