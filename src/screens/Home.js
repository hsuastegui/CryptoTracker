import React, { Component } from "react";
import { connect } from "react-redux";
import { StyleSheet, ScrollView, View } from "react-native";
import { Header, Icon, Text } from "react-native-elements";
import List from "../components/List";
// import { gdax } from "crypto-exchanges";

class Home extends Component {
  static navigationOptions = {
    title: "Home",
    tabBarLabel: "Home",
    tabBarIcon: ({ tintColor }) => <Icon name="home" style={{ color: tintColor }} />
  };
  async componentDidMount() {
    // const accounts = await gdax.getAccounts();
    // console.log(accounts);
  }
  render() {
    return (
      <View>
        <ScrollView>
          <Text h2 style={{ fontSize: 20 }}>
            Gdax
          </Text>
          <List data={this.props.data} config={{ hideChevron: true }} />
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  data: state.balance
});

export default connect(mapStateToProps)(Home);
