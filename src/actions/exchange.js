export const saveExchange = data => ({
  type: "SAVE_EXCHANGE",
  payload: data
});

export const removeExchange = id => ({
  type: "REMOVE_EXCHANGE",
  payload: id
});
