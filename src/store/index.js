import { createStore, applyMiddleware } from "redux";
import reducers from "../reducers";

const initialState = {
  balance: [
    {
      title: "BTC",
      subtitle: "123.40"
    },
    {
      title: "LTC",
      subtitle: "243211233.2"
    }
  ],
  exchange: [
    {
      id: 12,
      title: "GDAX"
    },
    {
      id: 45,
      title: "BITTREX"
    }
  ],
  orders: [
    {
      title: "GDAX",
      subtitle: "2 BTC at $45"
    },
    {
      title: "BITTREX",
      subtitle: "3.5 BTC at $392.2"
    }
  ]
};

const store = createStore(reducers, initialState);

export default store;
