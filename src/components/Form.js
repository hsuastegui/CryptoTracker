import React from "react";
import { StyleSheet, Text, View } from "react-native";
import {
  Header,
  Button,
  FormLabel,
  FormInput,
  FormValidationMessage
} from "react-native-elements";

export default class Form extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <FormLabel>Name</FormLabel>
        <FormInput
          onChangeText={() => {
            console.log("change");
          }}
        />
        <FormLabel>Other</FormLabel>
        <FormInput
          onChangeText={() => {
            console.log("change");
          }}
        />
        <FormValidationMessage>Error message</FormValidationMessage>
        <Button
          icon={{ name: "done", size: 32 }}
          buttonStyle={{ backgroundColor: "red" }}
          textStyle={{ textAlign: "center" }}
          title={`Save`}
          onPress={() => {
            console.log("press");
          }}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});
