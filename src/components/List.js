import React, { Component } from "react";
import { List, ListItem } from "react-native-elements";

const ListWrapper = ({ data, config }) => (
  <List containerStyle={{ marginBottom: 20 }}>
    {data.map((l, i) => (
      <ListItem key={i} {...l} leftIconOnPress={() => config.leftIconOnPress(l.id)} {...config} />
    ))}
  </List>
);

ListWrapper.defaultProps = {
  data: [],
  config: {}
};

export default ListWrapper;
