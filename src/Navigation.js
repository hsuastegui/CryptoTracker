import { TabNavigator, StackNavigator } from "react-navigation";
import Home from "./screens/Home";
import Orders from "./screens/Orders";
import Settings from "./screens/Settings";
import ExchangeOptions from "./screens/ExchangeOptions";

const TabNav = TabNavigator(
  {
    Home: {
      screen: Home,
      path: "/",
      navigationOptions: {
        tabBarLabel: "Home"
      }
    },
    Orders: {
      screen: Orders,
      path: "/orders",
      navigationOptions: {
        tabBarLabel: "Orders"
      }
    },
    Notifications: {
      screen: Settings,
      path: "/settings",
      navigationOptions: {
        tabBarLabel: "Settings"
      }
    }
  },
  {
    tabBarPosition: "bottom",
    animationEnabled: true,
    initialRouteName: "Home",
    tabBarOptions: {
      activeTintColor: "black",
      inactiveTintColor: "grey",
      style: { backgroundColor: "#3F6FC9" },
      labelStyle: {},
      tabStyle: {}
    }
  }
);

const StacksOverTabs = StackNavigator({
  Root: {
    screen: TabNav
  },
  ExchangeOptions: {
    screen: ExchangeOptions,
    path: "settings/exchange",
    navigationOptions: {
      title: "Exchange Configuration"
    }
  }
});

export default StacksOverTabs;
