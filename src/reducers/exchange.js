import { saveExchange } from "../utils/exchange";

const exchange = (state = [], action) => {
  switch (action.type) {
    case "SAVE_EXCHANGE":
      return saveExchange(state, action.payload);
    case "REMOVE_EXCHANGE":
      return state.filter(item => item.id !== action.payload);
    default: {
      return state;
    }
  }
};

export default exchange;
