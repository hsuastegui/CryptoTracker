const orders = (state = [], action) => {
  switch (action.type) {
    case "REMOVE_ORDER":
      return state.filter(item => item.id !== action.payload);
    default: {
      return state;
    }
  }
};

export default orders;
