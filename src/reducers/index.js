import { combineReducers } from "redux";
import balance from "./balance";
import exchange from "./exchange";
import orders from "./orders";

export default combineReducers({
  balance,
  exchange,
  orders
});
